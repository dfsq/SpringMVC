package com.dong.controller;

import com.dong.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ydw on 15/7/30.
 */
@Controller
@RequestMapping("/user")
public class UserController {

    private Map<String, User> users = new HashMap<String, User>();

    public UserController(){
        users.put("sqk", new User("sqk","123","dssdad@qq.com"));
        users.put("ydw", new User("ydw","123","2887382@qq.com"));
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String list(Model model){
        model.addAttribute("users", users);
        return "user/list";//服务器端跳转
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(@ModelAttribute("user") User user){
        // 开启ModelDriven
//        model.addAttribute(new User());
        return "user/add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(User user, String abc){
        users.put(user.getName(), user);
        return "redirect:/user/users";
    }

}
