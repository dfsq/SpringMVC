package com.dong.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by ydw on 15/7/29.
 */
@Controller
public class HelloController {

    @RequestMapping(method = RequestMethod.GET,value = "/hello")
    public String hello(){
        System.out.println("hello");
        return "hello";
    }
}
